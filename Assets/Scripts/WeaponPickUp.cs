﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPickUp : MonoBehaviour
{
    [SerializeField] private bool Active;
    [SerializeField] private Renderer Renderer;
    [SerializeField] private AudioSource Audio;

    public void SetActive(bool active)
    {
        Active = active;
        Renderer.enabled = active;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Active)
        {
            PlayerController Player = other.GetComponent<PlayerController>();
            if (Player != null)
            {
                Player.PickUpHammer();
                SetActive(false);
                Audio.Play();
            }
        }
        
    }



}
