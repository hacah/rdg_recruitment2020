﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class BestScores
{
    [SerializeField] public int[] Scores;

    public BestScores(int[] scores)
    {
        this.Scores = new int[10];
        for (int i = 0; i < scores.Length || i < 10; i++)
        {
            this.Scores[i] = scores[i];
        }

        Array.Sort(this.Scores);
    }

    public bool SubmitScore(int score)
    {
        int[] AllScores = new int[11];
        for (int i = 0; i < AllScores.Length; i++)
        {
            if (i == 10)
            {
                AllScores[i] = score;
            }
            else
            {
                AllScores[i] = Scores[i];
            }
        }

        Array.Sort(AllScores);
        Array.Reverse(AllScores);

        //Debug.Log("score: " + score);
        //Debug.Log("All Scores: ");

        for (int i = 0; i < Scores.Length; i++)
        {
            Scores[i] = AllScores[i];
            //Debug.Log(i+": "+AllScores[i]);
        }



        return !AllScores[10].Equals(score);
    }
}
