﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    [SerializeField] private Transform PipeEntrance;
    [SerializeField] private Transform PipeExit;
    private float LastActiavtionTime;
    [SerializeField] private float ActivationDelay;
    [SerializeField] private AudioSource Audio;

    private void Awake()
    {
        LastActiavtionTime = Time.realtimeSinceStartup;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Time.realtimeSinceStartup - LastActiavtionTime > ActivationDelay)
        {
            PlayerController player = other.GetComponent<PlayerController>();

            if (player != null)
            {
                player.GoThroughPipe(PipeEntrance.position.RoundToInt(), PipeExit.position.RoundToInt());
                LastActiavtionTime = Time.realtimeSinceStartup;
                Audio.Play();
            }
        }      
    }
}
