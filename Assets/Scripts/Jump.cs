﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{
    public Transform JumpTarget;
    public Transform JumpPlatform;
    public Vector3 PlayerPositionOffset;
    public float JumpSpeed;
    [SerializeField] private float ActivationDelay;
    [SerializeField] private Animator Anim;
    private float LastActiavtionTime;
    [SerializeField] private AudioSource Audio;

    private void Awake()
    {
        LastActiavtionTime = Time.realtimeSinceStartup;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Time.realtimeSinceStartup - LastActiavtionTime > ActivationDelay)
        {
            PlayerController player = other.GetComponent<PlayerController>();

            if (player != null)
            {
                Anim.SetTrigger("Jump");
                LastActiavtionTime = Time.realtimeSinceStartup;
                Audio.Play();
            }
        }
    }
}
