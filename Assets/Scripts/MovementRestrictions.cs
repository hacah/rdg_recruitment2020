﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MovementRestrictions
{
    public bool RestrictMovementXPlus;
    public bool RestrictMovementXMinus;
    public bool RestrictMovementZPlus;
    public bool RestrictMovementZMinus;

    public bool[] GetRestrictions()
    {
        return new bool[] { RestrictMovementXPlus, RestrictMovementXMinus, RestrictMovementZPlus, RestrictMovementZMinus };
    } 

    public MovementRestrictions(bool[] restrictions)
    {
        if (restrictions.Length == 4)
        {
            this.RestrictMovementXPlus = restrictions[0];
            this.RestrictMovementXMinus = restrictions[1];
            this.RestrictMovementZPlus = restrictions[2];
            this.RestrictMovementZMinus = restrictions[3];
        }
        else
        {
            throw new System.Exception("Wrong array length: " + restrictions.Length + " expected 4");
        }
    }
}
