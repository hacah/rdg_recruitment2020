﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpJumpBehaviour : StateMachineBehaviour
{
    private PlayerController Player;
    private Jump Jump;
    private bool Jumped = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Player = GameObject.FindObjectOfType<PlayerController>();
        Jump = animator.gameObject.GetComponent<Jump>();
        Player.ClearMovementStatus(false,true,true) ;
        Player.StopAllCoroutines();
        Jumped = false;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime < 0.5f)
        {
            Player.transform.position = Jump.JumpPlatform.position + Jump.PlayerPositionOffset;
        }
        else if(!Jumped)
        {
            Jumped = true;
            Player.StartCoroutine(Player.Move(Jump.JumpTarget.position, Jump.JumpSpeed));
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Jumped = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
