﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    private BestScores BestScores = new BestScores(new int[10]);
    private static string SaveScoresFolderPath = "/SavedData";
    private static string SaveScoresPath = SaveScoresFolderPath + "/Scores.json";
    private int Score = 0;
    private float TimeToReload = 3f;

    public void PlayerDied()
    {
        bool bestScore = BestScores.SubmitScore(Score);

        if (bestScore)
        {
            SaveScore();
        }

        GameObject.FindObjectOfType<PlayerController>().KillPlayer();
        GameObject.FindObjectOfType<UIManager>().DisplayGameOver(Score, BestScores.Scores);

        SaveScore();

        StartCoroutine(RestartGame());

        Time.timeScale = 0f;
    }

    private void Awake()
    {
        LoadScores();
    }

    private void LoadScores()
    {
        string PathToScores = Application.persistentDataPath + SaveScoresPath;
        if (File.Exists(PathToScores))
        {
            try
            {
                BestScores = JsonUtility.FromJson<BestScores>(File.ReadAllText(PathToScores));
            }
            catch (System.Exception)
            {
                BestScores = new BestScores(new int[10]);
            }
        }
    }

    private IEnumerator RestartGame()
    {
        yield return new WaitForSecondsRealtime(TimeToReload);

        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    private void SaveScore()
    {
        if (!Directory.Exists(Application.persistentDataPath + SaveScoresFolderPath))
        {
            Directory.CreateDirectory(Application.persistentDataPath + SaveScoresFolderPath);
        }
        string PathToScores = Application.persistentDataPath + SaveScoresPath;
        File.WriteAllText(PathToScores, JsonUtility.ToJson(BestScores));
    }

    public void AddScore(int score)
    {
        Score += score;
    }

    public int GetCurrentScore()
    {
        return Score;
    }

    public int[] GetBestScores()
    {
        return BestScores.Scores;
    }
}
