﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct WalkableCheckResult
{
    public PositionAccessiblity WalkableStatus;
    public MovingDirection MovingDirection;
    public MovementRestrictions Restrictions;

    public WalkableCheckResult(PositionAccessiblity status, MovingDirection direction, MovementRestrictions restrictions)
    {
        this.WalkableStatus = status;
        this.MovingDirection = direction;
        this.Restrictions = restrictions;
    }
}
