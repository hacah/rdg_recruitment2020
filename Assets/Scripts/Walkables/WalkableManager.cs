﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkableManager : MonoBehaviour
{
    private List<WalkableData> Walkables;
    

    private void Awake()
    {
        FindWalkables();
    }

    private void FindWalkables()
    {
        List<WalkableData> WalkablesDatas = new List<WalkableData>();

        Walkable[] WalkableArr = GameObject.FindObjectsOfType<Walkable>();
        for (int i = 0; i < WalkableArr.Length; i++)
        {
            if (!WalkableArr[i].IgnoreOnAwake)
            {
                WalkablesDatas.AddRange(WalkableArr[i].GetData());
            }
        }

        Walkables = WalkablesDatas;
    }

    public WalkableCheckResult CheckWalkable(Vector3Int position)
    {
        bool FoundHigher = false;
        bool FoundLower = false;
        int FoundHigherIndex = 0;
        int FoundLowerIndex = 0;
        MovingDirection movingDirection = MovingDirection.XMinusLeft;
        for (int i = 0; i < Walkables.Count; i++)
        {
            if (Walkables[i].Position == position)
            {
                //Debug.Log("Asked for: " + position + " --- Returned: " + PositionAccessiblity.EqualHeight.ToString());
                movingDirection = Walkables[i].MovingDirection;
                return new WalkableCheckResult(PositionAccessiblity.EqualHeight, movingDirection, Walkables[i].Restrictions);
            }
            if (Walkables[i].Position == (position + Vector3Int.up))
            {
                FoundHigherIndex = i;
                FoundHigher = true;
            }
            else if (Walkables[i].Position == (position + Vector3Int.down))
            {
                FoundLowerIndex = i;
                FoundLower = true;
            }
        }
        if (FoundHigher)
        {
            //Debug.Log("Asked for: " + position + " --- Returned: " + PositionAccessiblity.Higher.ToString());
            movingDirection = Walkables[FoundHigherIndex].MovingDirection;
            return new WalkableCheckResult(PositionAccessiblity.Higher, movingDirection, Walkables[FoundHigherIndex].Restrictions);
        }
        else if (FoundLower)
        {
            //Debug.Log("Asked for: " + position + " --- Returned: " + PositionAccessiblity.Lower.ToString());
            movingDirection = Walkables[FoundLowerIndex].MovingDirection;
            return new WalkableCheckResult(PositionAccessiblity.Lower, movingDirection, Walkables[FoundLowerIndex].Restrictions);
        }
        return new WalkableCheckResult(PositionAccessiblity.NotAvailable, movingDirection, new MovementRestrictions(new bool[] { false,false,false,false}));
    }

    public void RemoveWalkable(Vector3Int position)
    {
        for (int i = 0; i < Walkables.Count; i++)
        {
            if (Walkables[i].Position == position)
            {
                Walkables.RemoveAt(i);
                return;
            }
        }
    }

    public void AddWalkable(WalkableData walkable)
    {
        if (!Walkables.Contains(walkable))
        {
            Walkables.Add(walkable);
        }
    }

}
