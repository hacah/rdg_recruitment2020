﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct WalkableData
{
    public Vector3Int Position;
    public MovingDirection MovingDirection;
    public MovementRestrictions Restrictions;


    public WalkableData(Vector3Int position, MovingDirection movingDirection, MovementRestrictions restrictions)
    {
        this.Position = position;
        this.MovingDirection = movingDirection;
        this.Restrictions = restrictions;

    }

    public bool Equal(WalkableData other)
    {
        return (other.Position == this.Position) && (other.MovingDirection == this.MovingDirection) && (other.Restrictions == this.Restrictions);
    }
}
