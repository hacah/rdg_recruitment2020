﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walkable : MonoBehaviour
{
    public bool WalkableParent;
    public MovingDirection MovingDirection;
    public MovementRestrictions Restrictions;
    public bool IgnoreOnAwake = false;

    public WalkableData[] GetData()
    {  
        if (WalkableParent)
        {
            Transform[] children = GetComponentsInChildren<Transform>();
            WalkableData[] result = new WalkableData[children.Length - 1];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = new WalkableData(children[i+1].position.RoundToInt(), this.MovingDirection, this.Restrictions);
            }
            return result;
        }
        else
        {
            WalkableData[] result = { new WalkableData(this.transform.position.RoundToInt(), this.MovingDirection, this.Restrictions) };
            return result;
        }
        
    }
}
