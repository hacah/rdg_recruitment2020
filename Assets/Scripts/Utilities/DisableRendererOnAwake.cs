﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableRendererOnAwake : MonoBehaviour
{
    [SerializeField] private Renderer Renderer;

    private void Awake()
    {
        Renderer.enabled = false;
    }
}
