﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] private bool Active;
    [SerializeField] private int Score;
    [SerializeField] private Renderer Renderer;
    [SerializeField] private GameObject PickUpParticles;
    [SerializeField] private AudioSource Audio;

    public void Spawn()
    {
        if (!Active)
        {
            Active = true;
            Renderer.enabled = true;
            StartCoroutine(AnimateIn(1f));
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Active && other.GetComponent<PlayerController>() != null)
        {
            GameObject.FindObjectOfType<Game>().AddScore(Score);
            PickUp();
        }
    }

    private IEnumerator AnimateIn(float TotalTime)
    {
        float time = 0f;
        while (true)
        {
            time += Time.deltaTime;
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, time / TotalTime);
            if (time > TotalTime)
            {
                break;
            }
            yield return new WaitForEndOfFrame();
        }
    }

    private void PickUp()
    {
        Active = false;
        Renderer.enabled = false;
        Instantiate(PickUpParticles, transform.position + Vector3.up * 0.5f, Quaternion.identity);
        Audio.Play();
    }
}
