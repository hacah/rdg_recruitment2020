﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloaterController : MonoBehaviour
{
    [SerializeField] private Vector3 Target;
    [SerializeField] private WalkableManager WalkableManager;
    [SerializeField] private WalkableData BlockedLocation;
    [SerializeField] private float Speed;
    [SerializeField] private bool IsUp;
    [SerializeField] private float InitalDelay;
    public UnityEvent Moved = new UnityEvent();
    private Vector3 InitialPos;
    private float TimeNeeded;

    private void Awake()
    {
        InitialPos = transform.position;
        TimeNeeded = Vector3.Distance(InitialPos, Target) / Speed;
        if (WalkableManager == null)
        {
            WalkableManager = GameObject.FindObjectOfType<WalkableManager>();
        }
    }

    private void Start()
    {
        Move(InitalDelay);
    }

    public void Move(float delay)
    {
        StartCoroutine(ChangePosition(delay));
    }

    private IEnumerator ChangePosition(float delay)
    {
        yield return new WaitForSeconds(delay);

        float TimePassed = 0f;
        Vector3 StartingPoint;
        Vector3 Destination;

        if (IsUp)
        {
            StartingPoint = Target;
            Destination = InitialPos;

        }
        else
        {
            StartingPoint = InitialPos;
            Destination = Target;
            WalkableManager.AddWalkable(BlockedLocation);
        }

        while (TimePassed < TimeNeeded)
        {
            TimePassed += Time.deltaTime;
            transform.position = Vector3.Lerp(StartingPoint, Destination, TimePassed / TimeNeeded);
            yield return new WaitForEndOfFrame();
        }

        if (IsUp)
        {
            WalkableManager.RemoveWalkable(BlockedLocation.Position);          
        }

        Moved.Invoke();

        transform.position = Destination;

        IsUp = !IsUp;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (IsUp)
        {
            PlayerController Player = other.GetComponent<PlayerController>();
            if (Player != null)
            {
                Game game = GameObject.FindObjectOfType<Game>();
                game.PlayerDied();
            }
        }
    }
}
