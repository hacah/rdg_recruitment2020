﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TankController : MonoBehaviour
{
    [SerializeField] private Animator Anim;
    [SerializeField] private float Speed;
    public UnityEvent ReachedDestination = new UnityEvent();
    [SerializeField] Vector3[] Waypoints;
    [SerializeField] bool Looped;
    bool GoingBackwards = false;
    int Waypoint = 0;

    public void MoveToNextWayPoint(float delay)
    {
        if (Looped)
        {
            if (GoingBackwards)
            {
                if (Waypoint == 0)
                {
                    Waypoint++;
                    GoingBackwards = false;
                }
                else
                {
                    Waypoint--;
                }
            }
            else
            {
                if (Waypoint == Waypoints.Length - 1)
                {
                    Waypoint--;
                    GoingBackwards = true;
                }
                else
                {
                    Waypoint++;
                }
            }
        }
        else
        {
            Waypoint++;
            if (Waypoint == Waypoints.Length)
            {
                Waypoint = 0;
            }
        }

        StartCoroutine(Move(Waypoints[Waypoint], delay));
    }

    private IEnumerator Move(Vector3 destination, float delay)
    {
        yield return new WaitForSeconds(delay);

        Anim.SetBool("Moving",true);
        Vector3 InitialPos = transform.position;
        float TimeNeeded = Vector3.Distance(InitialPos, destination) / Speed;
        float TimePassed = 0f;
        while (TimePassed < TimeNeeded)
        {
            TimePassed += Time.deltaTime;
            transform.position = Vector3.Lerp(InitialPos, destination, TimePassed/TimeNeeded);
            yield return new WaitForEndOfFrame();
        }
        Anim.SetBool("Moving",false);
        transform.position = destination;
        ReachedDestination.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerController>() != null)
        {
            GameObject.FindObjectOfType<Game>().PlayerDied();
        }
    }


}
