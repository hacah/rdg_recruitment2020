﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MonsterController : MonoBehaviour
{
    public GameObject DeathParticles;
    public UnityEvent Death;
    [SerializeField] private Animator Anim;
    [SerializeField] private int ScoreForKilling;
    [SerializeField] private bool StartAlive;
    public Transform Body;
    private bool alive = false;

    // Start is called before the first frame update
    void Start()
    {
        if (StartAlive)
        {
            Show();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Die(float delay)
    {
        StartCoroutine(DelayedDeath(delay));
    }

    private IEnumerator DelayedDeath(float delay)
    {
        yield return new WaitForSeconds(delay);
        Die();
    }

    public void Die()
    {
        GameObject.FindObjectOfType<Game>().AddScore(ScoreForKilling);
        Death.Invoke();
        Anim.SetTrigger("Died");
        alive = false;
    }

    public void Show()
    {
        if (!alive)
        {
            Anim.SetTrigger("PopUp");
            alive = true;
        }
    }

    public bool IsAlive()
    {
        return alive;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (alive && other.GetComponent<PlayerController>() != null )
        {
            GameObject.FindObjectOfType<Game>().PlayerDied();
        }
    }
}
