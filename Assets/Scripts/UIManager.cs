﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI BestScores;
    [SerializeField] private TextMeshProUGUI CurrentScore;
    [SerializeField] private GameObject GameOverPanel;


    public void DisplayGameOver(int score, int[] bestScores)
    {
        CurrentScore.text = "Your Score:\n" + score;
        BestScores.text = "Best Scores:";

        for (int i = 0; i < bestScores.Length; i++)
        {
            BestScores.text += "\n" + (i+1) + ") " + bestScores[i];
        }

        GameOverPanel.SetActive(true);
    }
}
