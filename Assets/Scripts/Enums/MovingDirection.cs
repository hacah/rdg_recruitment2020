﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MovingDirection
{
    XPlusLeft,
    XMinusLeft,
    ZPlusLeft,
    ZMinusLeft
}
