﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum RotationNeeded
{
    None,
    Left,
    Right,
    Back
}
