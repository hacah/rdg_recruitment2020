﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PositionAccessiblity
{
    NotAvailable,
    EqualHeight,
    Lower,
    Higher
}
