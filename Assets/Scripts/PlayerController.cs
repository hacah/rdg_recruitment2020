﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private bool Moving
    {
        get
        {
            return _moving;
        }
        set
        {
            _moving = value;
        }
    }
    private bool Walking
    {
        get
        {
            return _walking;
        }
        set
        {
            _walking = value;
            Anim.SetBool("Walking", value);
        }
    }
    private bool _walking;
    private bool _moving = false;
    private bool Rotating = false;
    [Header("Speed settings")]
    [SerializeField] private float MovementSpeed;
    [SerializeField] private float FallingSpeed;
    private float DefaultMovementSpeed = 0.5f;
    private float DefaultFallingSpeed = 2f;
    private float AnimationSpeedMultiplier = 1f;
    [Header("Jump settings")]
    [SerializeField] private float JumpHeight;
    [SerializeField] private float JumpSpeed;
    private Vector3Int JumpDestination;
    [Header("Audio")]
    [SerializeField] private AudioSource Audio;
    [SerializeField] private AudioClip WalkingSound;
    [SerializeField] private AudioClip JumpingSound;
    [SerializeField] private AudioClip DeathSound;
    [SerializeField] private AudioClip HammerBreakingSound;
    [Header("References")]
    [SerializeField] private Animator Anim;
    [SerializeField] private PlayerInput Controls;
    public Transform HeroBody;
    [SerializeField] private GameObject Weapon;
    [SerializeField] private Renderer[] RenderersDisabledOnDeath;

    private bool HasHammer
    {
        get
        {
            return _hasHammer;
        }
        set
        {
            _hasHammer = value;
            if (!value && HammerHealth > 0)
            {
                HammerHealth = 0;
            }
            if (!value)
            {
                GameObject.FindObjectOfType<WeaponPickUp>().SetActive(true);
            }
            Weapon.SetActive(value);
        }
    }

    private int HammerHealth
    {
        get
        {
            return _hammerHealth;
        }
        set
        {
            //Debug.Log("Set Hammer Health to: " + value);
            if (value == 0)
            {
                _hammerHealth = 0;
                if (HasHammer)
                {
                    Audio.PlayOneShot(HammerBreakingSound);
                    HasHammer = false;
                }               
            }
            else if (value > 0)
            {
                _hammerHealth = value;
                if (!HasHammer)
                {
                    HasHammer = true;
                }            
            }
        }
    }

    private bool _hasHammer = false;
    private int _hammerHealth = 0;
    public Vector3 GetJumpDestination
    {
        get
        {
            return JumpDestination;
        }
    }
    
    private WalkableManager WalkableManager;
    private MovingDirection CameraMovingDirection
    {
        get
        {
            return _movingDirection;
        }
        set
        {
            _movingDirection = value;
        }
    }
    private MovingDirection _movingDirection = MovingDirection.XPlusLeft;
    [SerializeField] private MovingDirection LookingDirection;
    private bool Piping = false;

    [SerializeField] private GameObject DeathParticles;
    [SerializeField] private bool StaticMovement;

    private void Awake()
    {
        Controls = new PlayerInput();
        Controls.Enable();
        Controls.InGame.Movement.performed += input => Movement_performed(input.ReadValue<Vector2>());
        Controls.InGame.Escape.performed += exit => Application.Quit();
        MovementSpeed = MovementSpeed <= 0f ? DefaultMovementSpeed : MovementSpeed;
        FallingSpeed = FallingSpeed <= 0f ? DefaultFallingSpeed : FallingSpeed;
        Anim.speed = MovementSpeed * AnimationSpeedMultiplier;
        RefreshWalkableManager();
    }

    private void Movement_performed(Vector2 direction)
    {
        if (!Moving)
        {
            Vector3Int Destination = CalculateDestination(transform.position.RoundToInt(),direction.RoundToInt());

            if (!RestrictedDestination(Destination))
            {
                WalkableCheckResult DestinationResult = WalkableManager.CheckWalkable(Destination);

                PositionAccessiblity Accessibility = DestinationResult.WalkableStatus;

                if (Accessibility != PositionAccessiblity.NotAvailable)
                {
                    Moving = true;                  
                    //Debug.Log("Looking direction: " + LookingDirection);
                    RotationNeeded RotationNeeded = IsRotationNeeded(Destination);
                    LookingDirection = CalculateNewLookingDirection(Destination);
                    //Debug.Log("Rotation Needed: " + RotationNeeded);
                    CameraMovingDirection = DestinationResult.MovingDirection;
                    //RotateCamera();
                    if (RotationNeeded == RotationNeeded.None)
                    {
                        //Debug.Log("No rotation needed");
                        MoveUsingBestMethod(Destination, Accessibility);                     
                    }
                    else
                    {
                        //Debug.Log("Rotation needed: " + RotationNeeded);
                        StartCoroutine(RotateAndMove(Destination, Accessibility, RotationNeeded));
                    }
                }
                else
                {
                    Moving = false;
                }
                
            }
            
        }
    }

    private MovingDirection CalculateNewLookingDirection(Vector3Int destination)
    {
        Vector3Int pos = transform.position.RoundToInt();
        Vector3Int direction = destination - pos;
        if (direction.x == 1)
        {
            return MovingDirection.ZPlusLeft;
        }
        else if (direction.x == -1)
        {
            return MovingDirection.ZMinusLeft;
        }
        else if (direction.z == 1)
        {
            return MovingDirection.XMinusLeft;
        }
        else if (direction.z == -1)
        {
            return MovingDirection.XPlusLeft;
        }
        return LookingDirection;
    }

    public IEnumerator Move(Vector3 destination, float speed)
    {
        if (Mathf.RoundToInt(destination.y) == Mathf.RoundToInt(transform.position.y))
        {
            if (CanAttackEnemyAtDestination(destination.RoundToInt(), out MonsterController monster))
            {
                monster.Die(0.1f);
                Attack();
                yield return new WaitForSeconds(0.5f);
            }
            Audio.PlayOneShot(WalkingSound);
        }
        Moving = true;
        Walking = true;

        Vector3 PositionBefore = transform.position;

        float TimeSpent = 0f;
        float TimeToSpend = 1f / speed;

        while (TimeSpent < TimeToSpend)
        {
            transform.position = Vector3.Lerp(PositionBefore, destination, TimeSpent / TimeToSpend);
            TimeSpent += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        transform.position = destination;
        Walking = false;
        Moving = false;
    }

    private void Jump(Vector3Int destination)
    {
        JumpDestination = destination;
        Anim.SetTrigger("Jump");
        Audio.PlayOneShot(JumpingSound);
    }

    public void FinishJump()
    {
        if (!Piping)
        {
            transform.position = JumpDestination;
            Moving = false;
        }
    }

    private IEnumerator Fall(float delay = 0f)
    {
        yield return new WaitForSeconds(delay);

        while (WalkableManager.CheckWalkable(transform.position.RoundToInt()).WalkableStatus != PositionAccessiblity.EqualHeight )
        {
            Moving = true;
            StartCoroutine(Move(transform.position.RoundToInt() + Vector3Int.down, FallingSpeed));
            yield return new WaitForSeconds(1f/FallingSpeed);
        }

        //Moving = false;
    }

    private void RefreshWalkableManager()
    {
        WalkableManager = GameObject.FindObjectOfType<WalkableManager>();
    }

    private Vector3Int CalculateDestination(Vector3Int start, Vector2Int direction)
    {
        Vector3Int Destination = start;
        MovingDirection Reference = StaticMovement ? MovingDirection.XPlusLeft : CameraMovingDirection;
        switch (Reference)
        {
            case MovingDirection.XPlusLeft:
                Destination += new Vector3Int(
                -direction.x, 0, -direction.y);
                break;
            case MovingDirection.XMinusLeft:
                Destination += new Vector3Int(
                direction.x, 0, direction.y);
                break;
            case MovingDirection.ZPlusLeft:
                Destination += new Vector3Int(
                direction.y, 0, -direction.x);
                break;
            case MovingDirection.ZMinusLeft:
                Destination += new Vector3Int(
                -direction.y, 0, direction.x);
                break;
            default:
                break;
        }
        return Destination;
    } 

    private bool RestrictedDestination(Vector3Int destination)
    {
        MovementRestrictions restrictions = WalkableManager.CheckWalkable(transform.position.RoundToInt()).Restrictions;

        Vector3Int direction = destination - transform.position.RoundToInt();

        if (direction.x == 1)
        {
            return restrictions.RestrictMovementXPlus;
        }
        else if (direction.x == -1)
        {
            return restrictions.RestrictMovementXMinus;
        }
        else if (direction.z == 1)
        {
            return restrictions.RestrictMovementZPlus;
        }
        else if (direction.z == -1)
        {
            return restrictions.RestrictMovementZMinus;
        }
        else
        {
            return false;
        }
    }

    private IEnumerator RotateAndMove(Vector3Int destination, PositionAccessiblity accessiblity, RotationNeeded rotationNeeded)
    {     
        if (rotationNeeded != RotationNeeded.None)
        {
            Rotating = true;
            Anim.SetTrigger("Rotate");
            if (rotationNeeded == RotationNeeded.Left)
            {
                Anim.SetTrigger("RotateLeft");
            }
            else if (rotationNeeded == RotationNeeded.Right)
            {
                Anim.SetTrigger("RotateRight");
            }
            else if (rotationNeeded == RotationNeeded.Back)
            {
                Anim.SetTrigger("RotateBack");
            }
            yield return new WaitWhile(() => Rotating);
        }

        MoveUsingBestMethod(destination, accessiblity);        
    }
    
    public void FinishRotation()
    {
        //Debug.Log("Looking dir after rotation: " + LookingDirection);
        switch (LookingDirection)
        {
            case MovingDirection.XPlusLeft:
                transform.eulerAngles = new Vector3(0f, 0, 0f);
                break;
            case MovingDirection.XMinusLeft:
                transform.eulerAngles = new Vector3(0f, 180f, 0f);
                break;
            case MovingDirection.ZPlusLeft:
                transform.eulerAngles = new Vector3(0f, -90f, 0f);
                break;
            case MovingDirection.ZMinusLeft:
                transform.eulerAngles = new Vector3(0f, 90, 0f);
                break;
            default:
                break;
        }
        Rotating = false;
    }


    private void MoveUsingBestMethod(Vector3Int destination, PositionAccessiblity accessibility)
    {
        if (accessibility == PositionAccessiblity.EqualHeight)
        {
            StartCoroutine(Move(destination, MovementSpeed));
        }
        else if (accessibility == PositionAccessiblity.Higher)
        {
            destination += Vector3Int.up;
            Jump(destination);
        }
        else if (accessibility == PositionAccessiblity.Lower)
        {
            StartCoroutine(Move(destination, MovementSpeed));
            StartCoroutine(Fall(1f / MovementSpeed));
        }
    }

    private RotationNeeded IsRotationNeeded(Vector3Int destination)
    {
        Vector3Int direction = destination - transform.position.RoundToInt();
        if (direction.x == -1)
        {
            switch (LookingDirection)
            {
                case MovingDirection.XPlusLeft:
                    return RotationNeeded.Right;
                case MovingDirection.XMinusLeft:
                    return RotationNeeded.Left;
                case MovingDirection.ZPlusLeft:
                    return RotationNeeded.Back;
                case MovingDirection.ZMinusLeft:
                    return RotationNeeded.None;
                default:
                    break;
            }
        }
        else if (direction.x == 1)
        {
            switch (LookingDirection)
            {
                case MovingDirection.XPlusLeft:
                    return RotationNeeded.Left;
                case MovingDirection.XMinusLeft:
                    return RotationNeeded.Right;
                case MovingDirection.ZPlusLeft:
                    return RotationNeeded.None;
                case MovingDirection.ZMinusLeft:
                    return RotationNeeded.Back;
                default:
                    break;
            }
        }
        else if (direction.z == 1)
        {
            switch (LookingDirection)
            {
                case MovingDirection.XPlusLeft:
                    return RotationNeeded.Back;
                case MovingDirection.XMinusLeft:
                    return RotationNeeded.None;
                case MovingDirection.ZPlusLeft:
                    return RotationNeeded.Left;
                case MovingDirection.ZMinusLeft:
                    return RotationNeeded.Right;
                default:
                    break;
            }
        }
        else if (direction.z == -1)
        {
            switch (LookingDirection)
            {
                case MovingDirection.XPlusLeft:
                    return RotationNeeded.None;
                case MovingDirection.XMinusLeft:
                    return RotationNeeded.Back;
                case MovingDirection.ZPlusLeft:
                    return RotationNeeded.Right;
                case MovingDirection.ZMinusLeft:
                    return RotationNeeded.Left;
                default:
                    break;
            }
        }

        return RotationNeeded.None;
    }

    public void GoThroughPipe(Vector3Int pipeEntrancePos, Vector3Int pipeExitPos)
    {
        if (!Piping)
        {
            Piping = true;
            StartCoroutine(PipeMovement(pipeEntrancePos, pipeExitPos));
        }       
    }

    private IEnumerator PipeMovement(Vector3Int pipeEntrancePos, Vector3Int pipeExitPos)
    {
        float Time = 0f;
        float Duration = 0.5f;
        Vector3 InitialPos = transform.position;
        while (Time < Duration)
        {
            transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, Time / Duration);
            transform.position = Vector3.Lerp(InitialPos, pipeEntrancePos, Time / Duration);
            Time += UnityEngine.Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Time = 0f;
        transform.localScale = Vector3.zero;
        transform.position = pipeExitPos;

        StartCoroutine(Fall(0f));
        while (Time < Duration)
        {
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, Time / Duration);
            Time += UnityEngine.Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        Piping = false;
        

    }

    public void ClearMovementStatus()
    {
        Piping = false;
        Walking = false;
        Moving = false;
    }

    public void ClearMovementStatus(bool moving, bool walking, bool piping)
    {
        if (moving)
        {
            Moving = false;
        }
        if (walking)
        {
            Walking = false;
        }
        if (piping)
        {
            Piping = false;
        }
    }

    public void KillPlayer()
    {
        Audio.PlayOneShot(DeathSound);
        Controls.Dispose();
        Instantiate(DeathParticles, HeroBody.position, Quaternion.identity);
        //Renderer.enabled = false;
        //this.gameObject.SetActive(false);
        for (int i = 0; i < RenderersDisabledOnDeath.Length; i++)
        {
            RenderersDisabledOnDeath[i].enabled = false;
        }
    }

    private void Attack()
    {
        //Debug.Log("Attack");
        ClearMovementStatus();
        Moving = true;
        Anim.SetTrigger("Attack");
    }

    private bool CanAttackEnemyAtDestination(Vector3Int destination, out MonsterController monster)
    {
        //Debug.Log("Checking if can attack enemy");

        if (HammerHealth == 0 || !HasHammer)
        {
            //Debug.Log("Hammer: " + HasHammer + "   " + HammerHealth);
            monster = null;
            return false;
        }

        MonsterController[] Monsters = GameObject.FindObjectsOfType<MonsterController>();

        for (int i = 0; i < Monsters.Length; i++)
        {
            if (Monsters[i].IsAlive() && Monsters[i].transform.position.RoundToInt() == destination)
            {
                monster = Monsters[i];
                return true;
            }
        }

        monster = null;
        return false;
    }

    public void DamageHammer(int amount)
    {
        HammerHealth -= amount;

    }

    public void PickUpHammer()
    {
        HammerHealth = 2;
    }

}
